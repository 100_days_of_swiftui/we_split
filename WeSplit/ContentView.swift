//
//  ContentView.swift
//  We Split
//
//  Created by Hariharan S on 30/04/24.
//

import SwiftUI

struct ContentView: View {
    @State private var checkAmount = 0.0
    @State private var numberOfPeople = 0
    @State private var tipPercentage = 0
    @FocusState private var isAmountTextFieldFocused: Bool
    
    var totalAmount: Double {
        let tipSelection = Double(self.tipPercentage)
        let tipValue = self.checkAmount / 100 * tipSelection
        let grandTotal = self.checkAmount + tipValue
        return grandTotal
    }
    var totalPerPerson: Double {
        let peopleCount = Double(self.numberOfPeople + 2)
        let amountPerPerson = self.totalAmount / peopleCount
        return amountPerPerson
    }
    
    var body: some View {
        NavigationStack {
            Form {
                Section("Enter the Amount") {
                    TextField("0", value: self.$checkAmount, format: .currency(code: Locale.current.currency?.identifier ?? "INR"))
                        .keyboardType(.decimalPad)
                        .focused(self.$isAmountTextFieldFocused)
                }
                
                Section("How much tip do you want to leave?") {
                    Picker("Tip percentage", selection: $tipPercentage) {
                        ForEach(0..<101, id: \.self) {
                            Text($0, format: .percent)
                        }
                    }
                    .pickerStyle(.navigationLink)
                }
                
                Section {
                    Picker("Number of People", selection: self.$numberOfPeople) {
                        ForEach(2..<100) {
                            Text("\($0)")
                        }
                    }
                }
                
                Section("Total Amount") {
                    Text(self.totalAmount, format: .currency(code: Locale.current.currency?.identifier ?? "INR"))
                        .foregroundStyle(self.tipPercentage == 0 ? .red : .black)
                }
                
                Section("Amount Per Person") {
                    Text(self.totalPerPerson, format: .currency(code: Locale.current.currency?.identifier ?? "INR"))
                }
            }
            .navigationTitle("We Split")
            .toolbar {
                if self.isAmountTextFieldFocused {
                    Button("Done") {
                        self.isAmountTextFieldFocused = false
                    }
                }
            }
        }
    }
}

#Preview {
    ContentView()
}
