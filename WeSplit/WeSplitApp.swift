//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Hariharan S on 01/05/24.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
